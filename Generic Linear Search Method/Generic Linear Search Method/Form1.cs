﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generic_Linear_Search_Method
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Focus(); 
        }
    
        Random rand = new Random();
        char Letter = new char();
        int[] Int_TArray = new int[10] { 4, 3, 6, 7, 8, 9, 0, 2, 1, 5 };
        char[] Char_TArray = new char[13] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'p', 's', 'v', 'x', 'r', 'y' };


        private void IntegerButton_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(rand.Next(5) + 1);
            ButtonChar.Enabled = false;
        }

        private void ButtonChar_Click(object sender, EventArgs e)
        {
            IntegerButton.Enabled = false;
            switch (rand.Next(5))
            {
                case 1:
                    Letter = 'w';
                    break;
                case 2:
                    Letter = 's';
                    break;
                case 3:
                    Letter = 'l';
                    break;
                case 4:
                    Letter = 'x';
                    break;
                case 5:
                    Letter = 'h';
                    break;
                case 6:
                    Letter = 'b';
                    break;
            }
            listBox1.Items.Add(Letter.ToString());
        }

        private void ButtonSelectedItem_Click(object sender, EventArgs e)
        {
            switch (IntegerButton.Enabled)
            {
                case false:
                    foreach (char b in listBox1.SelectedItem.ToString())
                    {
                        GenericSearch(listBox1.SelectedItem.ToString()[b], Char_TArray);
                    }
                    break;
                case true:
                    int Proceed;
                    int.TryParse(listBox1.SelectedItem.ToString(), out Proceed);
                    GenericSearch(Proceed, Int_TArray);
                    break;
            }
            IntegerButton.Enabled = true;
            ButtonChar.Enabled = true;
        }
        private void GenericSearch<T>(T SearchKey, T[] TArray) where T : IComparable<T>
        {

            for (int x = 0; x < TArray.Length; ++x)
            {
                if (SearchKey.CompareTo(TArray[x]) == 0)
                {
                    textBox2.Text = $"Found it! at {x}th/rd/nd/st Position";
                    break;
                }
                else textBox2.Text = $"Sorry: {SearchKey} wasn't found.";

            }
        }
    }
}
