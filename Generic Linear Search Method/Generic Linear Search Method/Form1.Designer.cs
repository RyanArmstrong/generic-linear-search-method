﻿namespace Generic_Linear_Search_Method
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IntegerButton = new System.Windows.Forms.Button();
            this.ButtonChar = new System.Windows.Forms.Button();
            this.ButtonSelectedItem = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // IntegerButton
            // 
            this.IntegerButton.Location = new System.Drawing.Point(12, 41);
            this.IntegerButton.Name = "IntegerButton";
            this.IntegerButton.Size = new System.Drawing.Size(128, 44);
            this.IntegerButton.TabIndex = 0;
            this.IntegerButton.Text = "Generate Random Integers";
            this.IntegerButton.UseVisualStyleBackColor = true;
            this.IntegerButton.Click += new System.EventHandler(this.IntegerButton_Click);
            // 
            // ButtonChar
            // 
            this.ButtonChar.Location = new System.Drawing.Point(146, 41);
            this.ButtonChar.Name = "ButtonChar";
            this.ButtonChar.Size = new System.Drawing.Size(128, 44);
            this.ButtonChar.TabIndex = 1;
            this.ButtonChar.Text = "Generate Random Character";
            this.ButtonChar.UseVisualStyleBackColor = true;
            this.ButtonChar.Click += new System.EventHandler(this.ButtonChar_Click);
            // 
            // ButtonSelectedItem
            // 
            this.ButtonSelectedItem.Location = new System.Drawing.Point(52, 243);
            this.ButtonSelectedItem.Name = "ButtonSelectedItem";
            this.ButtonSelectedItem.Size = new System.Drawing.Size(177, 55);
            this.ButtonSelectedItem.TabIndex = 3;
            this.ButtonSelectedItem.Text = "Search for selected Item";
            this.ButtonSelectedItem.UseVisualStyleBackColor = true;
            this.ButtonSelectedItem.Click += new System.EventHandler(this.ButtonSelectedItem_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 313);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(343, 20);
            this.textBox2.TabIndex = 4;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 105);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(262, 134);
            this.listBox1.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 356);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.ButtonSelectedItem);
            this.Controls.Add(this.ButtonChar);
            this.Controls.Add(this.IntegerButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button IntegerButton;
        private System.Windows.Forms.Button ButtonChar;
        private System.Windows.Forms.Button ButtonSelectedItem;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ListBox listBox1;
    }
}

